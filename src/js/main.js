var $ = require('jquery');
var viewport = require('jquery.viewport');
var foundation = require('foundation');

$(document).foundation();
$(document).foundation('interchange', 'reflow');



// $(document).ready(function() {
//     var inner = $("video");
//     var elementPosTop = inner.position().top;
//     var viewportHeight = $(window).height();
//     $(window).on('scroll', function() {
//         var scrollPos = $(window).scrollTop();
//         var elementFromTop = elementPosTop - scrollPos;

//         if (elementFromTop > 0 && elementFromTop < elementPosTop + viewportHeight) {
//             inner.get(0).play();
//         } else {
//             inner.get(0).pause();
//         }
//     });
// })
$(window).on('beforeunload', function() { $(".logo-video").hide(); });
$(document).ready(function(){
	$(window).scroll(function(){
	    
	    $( ".bodyvid:in-viewport" ).each(function( index ) {
	      // console.log( index + ": " + $( this ).text() );
	      if (!$(this).hasClass('active')){
	      	$(this)[0].play();
	      	$(this).addClass('active');
	      }
	      
	      
	    });
	});
	$('#nav-toggle').on('click', function(e){
		e.preventDefault();
		$('#conversationsMenu').toggleClass('active');
		$('.overlay').toggleClass('active');
		$('#nav-toggle').toggleClass('active');
	});
	$('a[href*=#]:not([href=#])').click(function() {
	  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    var target = $(this.hash);
	    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    if (target.length) {
	      $('html,body').animate({
	        scrollTop: target.offset().top
	      }, 1000);
	      return false;
	    }
	  }
	});
});



